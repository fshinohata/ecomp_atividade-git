# Atividade de Git

Este projeto é uma atividade de git, feita para você exercitar alguns conceitos e passar por todas as funcionalidades básicas do git.

Este documento contém todas as instruções (em forma de roteiro) para você seguir e relembrar os conceitos estudados.

Se você não quiser seguir o roteiro para relembrar os conceitos, você pode [pular direto para a atividade](#atividade-pós-roteiro).

**NOTA:** Não faça uma simples cópia dos comandos. Leia-os com cuidado, pois alguns requerem que você escreva coisas diferentes, como seu nome. Atenção aos comandos que se assemelhem aos itens abaixo:

```
seu_nome_completo
seu_usuario_do_git
seu_numero_do_cartao
seu_cpf_e_rg
```

Siga todos os passos e complete a historinha!

## Roteiro

Você é **Mathias, o ecompinguim**, um desbravador do mundo da computação e sofredor com certificado. A seguinte história conta uma de suas aventuras, desta vez em um lendário mundo trazido pela ferramenta `git`.

### Capítulo 1: Clonando o projeto

Mathias estava mexendo em seu GNU/Linux/Fedora/Manjaro/Arch Linux/FreeBSD/Unix, e um dia resolveu se aventurar no mundo do git. Então, ele resolveu clonar o projeto da atividade de git rodando este comando em seu terminal *bash*:

```bash
mathias$ git clone https://seu_usuario_do_git@gitlab.com/fshinohata/ecomp_atividade-git.git atividade_de_git_do_mathias
```

Então, Mathias entrou na pasta `atividade_de_git_do_mathias/` que foi criada depois de seu `git clone`, e percebeu que o único arquivo que estava lá é o `README.md`.

Isso deixou Mathias triste, porque ele não podia fazer nada do incrível mundo do git só com o arquivo `README.md`. Com isso, ele resolveu criar seus próprios arquivos e fazer seus próprios testes, para aprender a usar o git e tudo que a ferramenta podia oferecer para ele.

Mas antes de tudo, Mathias precisava dizer ao `git` quem ele era naquele projeto. Então, ele abriu um terminal na pasta `atividade_de_git_do_mathias/` e disse ao `git` quem ele era:

```bash
mathias$ git config --local user.name "seu_nome_completo"
mathias$ git config --local user.email "seu_email_que_voce_usou_no_cadastro_do_git"
```

### Capítulo 2: Criando arquivos na branch errada

O primeiro teste de Mathias foi criar arquivos. Então, ele criou seu primeiro arquivo e escreveu uma mensagem nele:

```bash
# O comando touch cria arquivos vazios, e o echo "mensagem" escreve a mensagem no terminal.
# O uso do '>' depois do echo faz ele redirecionar a mensagem "mensagem" para o arquivo, ao invés de escrever no terminal
mathias$ touch meu_arquivo.txt
mathias$ echo "OLÁ, MUNDO" > meu_arquivo.txt
```

Depois disso, Mathias percebeu um problema: ele ainda estava na branch `master`!

```bash
mathias$ git branch
* master
```

Mathias lembrou que uma vez disseram que a branch `master` deveria ter apenas versões "estáveis" de um projeto. Como ele ainda queria fazer vários testes, ele não poderia estar ali!

Mas e agora? O que fazer?

### Capítulo 3: Arrumando a bagunça

Enquanto matutava sobre a vida, Mathias de repente se lembrou que o seu novo arquivo não estava sendo monitorado!

```bash
mathias$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	meu_arquivo.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

Como arquivos não-monitorados não são ligados à branchs, Mathias poderia simplesmente mudar para outra branch e seu arquivo seria "passado" para a outra, sem afetar a `master`!

Mathias não pensou duas vezes, e criou uma branch nova para que ele pudesse fazer seus testes sem medo de quebrar a `master`:

```bash
# A opção -b faz o checkout criar a branch se nao existir
# O comando equivalente eh 'git branch branch_nova branch_pai', que nesse caso seria 'git branch minha_primeira_branch master'
mathias$ git checkout -b minha_primeira_branch
mathias$ git branch
  master
* minha_primeira_branch
```

Agora não há problemas. Só falta uma coisa: a branch nova do Mathias foi criada apenas na máquina, e não foi criada no repositório online do `gitlab` ainda. Ele se certificou disso pedindo para ver que branches existiam no repositório:

```bash
mathias$ git branch -r
  origin/HEAD -> origin/master
  origin/master
```

Mas resolver isso é fácil!

O passo-a-passo já era parte do corpo de Mathias. Bastou colocar suas nadadeiras no teclado e elas já sabiam o que fazer.

Primeiro, Mathias adicionou seu arquivo `meu_arquivo.txt` para ser monitorado e marcado como modificado:

```bash
# Nota: use 'git add .', E EVITE 'git add *', se quiser adicionar todos os arquivos alterados de uma vez!
mathias$ git add meu_arquivo.txt
mathias$ git status
On branch minha_primeira_branch
Your branch is up-to-date with 'origin/minha_primeira_branch'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   meu_arquivo.txt

```

O próximo passo é "descrever quais foram as modificações", pois o `git` não deixa que você simplesmente adicione arquivos novos ou altere existentes sem dizer qual é o seu plano malígno. Mathias, como um bom programador, descreveu sua atividade nos mínimos detalhes:

```bash
mathias$ git commit -m "Fiz um arquivo meu. SÓ MEU."
mathias$ git status
On branch minha_primeira_branch
nothing to commit, working directory clean
```

Por último, Mathias enviou suas mudanças para o repositório, resultando na criação de sua branch nova e na atualização do seu arquivo ao mesmo tempo:

```bash
mathias$ git push origin minha_primeira_branch
...
To https://seu_usuario_do_git@gitlab.com/fshinohata/ecomp_atividade-git.git
 * [new branch]      minha_primeira_branch -> minha_primeira_branch

mathias$ git branch -r
  origin/HEAD -> origin/master
  origin/master
  origin/minha_primeira_branch
```

E com isso, Mathias terminou seu primeiro teste!

Até este capítulo, ele já relembrou como criar branches, o que são arquivos não monitorados, e como saber se o repositório está atualizado ou não. 

(Se você ainda não entendeu algum dos conceitos acima, pesquise e/ou faça mais testes e veja as coisas funcionando!)

### Capítulo 4: Entendendo branches e o fazendo o primeiro merge

O próximo teste que Mathias decidiu fazer tem como objetivo entender o que é uma branch de fato. Seus amigos disseram uma vez que uma branch é como uma pasta separada, contendo uma cópia do projeto para você fazer suas alterações, e o nome da pasta é o nome da branch.

Os amigos de Mathias ainda complementaram que o `git merge` (que envolve apenas duas branches) é a mesma coisa que pegar duas pastas com versões diferentes e tentar mesclar tudo e fazer tudo funcionar com as mudanças feitas nas duas pastas. A única diferença é que o `git` faz isso automagicamente, fato que deixou Mathias fascinado!

Para testar esses conceitos, Mathias teve uma ideia: ele decidiu criar um novo arquivo e escrever um texto bem legal, e depois criar uma branch nova para fazer alterações nesse texto bem legal para então realizar o merge entre a branch nova e a branch "minha\_primeira\_branch"!

Então, Mathias criou seu arquivo:

```bash
mathias$ touch um_texto_bem_legal.txt
```

Dentro do arquivo, ele colocou o seguinte texto:

```
# LERO LERO PARA COLOCAR NOS MEUS TRABALHOS

Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a hegemonia do ambiente político maximiza as possibilidades por conta de alternativas às soluções ortodoxas.

O incentivo ao avanço tecnológico, assim como o consenso sobre a necessidade de qualificação representa uma abertura para a melhoria dos níveis de motivação departamental.

A certificação de metodologias que nos auxiliam a lidar com a percepção das dificuldades acarreta um processo de reformulação e modernização dos níveis de motivação departamental.



# FRASES E POEMAS LEGAIS

A batata

  Rosas são vermelhas
  Violetas são roxas
  E batatas são tubérculos

O tubérculo

  Rosas são vermelhas
  Violetas são roxas
  E minha mamãe vai fazer batata frita hoje no almoço
```

Em seguida, ele salvou seu arquivo adicionando-o na branch com `git add` (porque ele ainda é não-monitorado), "commitou" suas mudanças e salvou tudo no repositório para não perder seu texto caso seu computador pegasse fogo espontaneamente:

```bash
mathias$ git add um_texto_bem_legal.txt
mathias$ git commit -m "Fiz um texto bem legal e coloquei no arquivo 'um_texto_bem_legal.txt'. NOTA: NÃO É UM TEXTO!"
mathias$ git push origin minha_primeira_branch
```

Então, Mathias prosseguiu com seu teste e criou sua branch nova:

```bash
mathias$ git checkout -b branch_para_fazer_merge_com_a_minha_primeira_branch
```

...E alterou seu texto no arquivo 'um\_texto\_bem\_legal.txt' com ideias novas que surgiram em sua cabeça nesse período de tempo:

```
# LERO LERO PARA COLOCAR NOS MEUS TRABALHOS

Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a hegemonia do ambiente político maximiza as possibilidades por conta de alternativas às soluções ortodoxas.

O incentivo ao avanço tecnológico, assim como o consenso sobre a necessidade de qualificação representa uma abertura para a melhoria dos níveis de motivação departamental.

# NÃO GOSTEI DESSE AQUI, NÃO VOU USAR MAS NÃO QUERO TIRAR TAMBÉM
A certificação de metodologias que nos auxiliam a lidar com a percepção das dificuldades acarreta um processo de reformulação e modernização dos níveis de motivação departamental.



# FRASES E POEMAS LEGAIS

A batata

  Rosas são vermelhas
  Violetas são roxas
  E batatas são legais

O tubérculo

  Rosas são vermelhas
  Violetas são roxas
  E eu vou comer batata frita hoje no almoço
```

Assim que terminou de editar o arquivo, a mãe de Mathias chamou ele pra almoçar. Quando ele voltou, ele tinha esquecido o que ele havia mudado, e queria relembrar. Pra isso, ele executou o comando:

```bash
mathias$ git diff um_texto_bem_legal.txt
```

Quando terminou de ler as mudanças, Mathias adicionou e commitou suas mudanças na branch 'branch\_para\_fazer\_merge\_com\_a\_minha\_primeira\_branch':

```bash
mathias$ git add um_texto_bem_legal.txt
mathias$ git commit -m "Mudei meu texto bem legal. Mudei os poemas. Tinha uma frase de lero lero que eu não gostei, mas eu não tirei, mas eu marquei qual foi."
mathias$ git push origin branch_para_fazer_merge_com_a_minha_primeira_branch
```

E para finalizar o teste, Mathias voltou para sua branch 'minha\_primeira\_branch'. Mas antes de realizar o merge, lembrou de uma coisa que ele precisava mudar no arquivo, e o deixou assim:

```
# LERO LERO PARA COLOCAR NOS MEUS TRABALHOS

Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a hegemonia do ambiente político maximiza as possibilidades por conta de alternativas às soluções ortodoxas.

O incentivo ao avanço tecnológico, assim como o consenso sobre a necessidade de qualificação representa uma abertura para a melhoria dos níveis de motivação departamental.

# NÃO GOSTEI DESSE AQUI, NÃO VOU USAR MAS NÃO QUERO TIRAR TAMBÉM
A certificação de metodologias que nos auxiliam a lidar com a percepção das dificuldades acarreta um processo de reformulação e modernização dos níveis de motivação departamental.



# FRASES E POEMAS LEGAIS

A batata

  Rosas são vermelhas
  Violetas são roxas
  E batatas são legais

O tubérculo

  Rosas são vermelhas
  Violetas são roxas
  E eu comi batata frita hoje no almoço
```

E o salvou em seguida:

```bash
mathias$ git add .
mathias$ git commit -m "Mudei meu texto bem legal de novo. Hoje eu comi batata frita no almoço."
mathias$ git push origin minha_primeira_branch
```

Finalmente, Mathias fez o merge para ver o que acontecia com seu texto. Como o `git merge <branch>` faz o merge entre "a branch que você está agora" e "a branch que você colocou o nome na frente do comando", Mathias executou o comando desse jeito:

```bash
mathias$ git merge branch_para_fazer_merge_com_a_minha_primeira_branch
Auto-merging um_texto_bem_legal.txt
CONFLICT (content): Merge conflict in um_texto_bem_legal.txt
Automatic merge failed; fix conflicts and then commit the result.
```

Quando Mathias executou o comando, ocorreu um conflito dentro do arquivo 'um\_texto\_bem\_legal.txt'!

Mathias foi ver o que tinha acontecido, e o arquivo ficou assim:

```
1    # LERO LERO PARA COLOCAR NOS MEUS TRABALHOS
2    
3    Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a hegemonia do ambiente político maximiza as possibilidades por conta de alternativas às soluções ortodoxas.
4    
5    O incentivo ao avanço tecnológico, assim como o consenso sobre a necessidade de qualificação representa uma abertura para a melhoria dos níveis de motivação departamental.
6    
7    # NÃO GOSTEI DESSE AQUI, NÃO VOU USAR MAS NÃO QUERO TIRAR TAMBÉM
8    A certificação de metodologias que nos auxiliam a lidar com a percepção das dificuldades acarreta um processo de reformulação e modernização dos níveis de motivação departamental.
9    
10    
11    
12   # FRASES E POEMAS LEGAIS
13   
14   A batata
15   
16     Rosas são vermelhas
17     Violetas são roxas
18     E batatas são legais
19    
20   O tubérculo
21    
22     Rosas são vermelhas
23     Violetas são roxas
24   <<<<<<< HEAD
25     E eu comi batata frita hoje no almoço
26   =======
27     E eu vou comer batata frita hoje no almoço
28   >>>>>>> branch_para_fazer_merge_com_a_minha_primeira_branch
```

Lendo o arquivo, Mathias percebeu três pontos interessantes:

1. O seu comentário na linha 7, que não existia, foi colocado sem problemas;
2. A sua mudança de 'batatas são tubérculos' para 'batatas são legais' na linha 18 aconteceu sem problemas, porque o texto 'batatas são legais' já era do arquivo original e não foi alterado;
3. As suas mudanças 'E eu comi batata frita hoje no almoço' e 'E eu vou comer batata frita hoje no almoço' deram conflito, porque os dois são alterações diferentes da frase original 'E minha mamãe vai fazer batata frita hoje no almoço'. Além disso, os dois textos que geraram o conflito foram demarcados pelo git (<<<<<<< HEAD quer dizer "o que tem na sua branch atual", o ======= é um separador dos textos e o >>>>>>> <branch> quer dizer "o que tinha na branch <branch>")

Como Mathias já havia almoçado, ele não queria manter a frase 'E eu vou comer batata frita hoje no almoço'. Então, ele simplesmente apagou ela junto com as marcações feitas pelo `git` para indicar os locais de conflito:

```
20   O tubérculo
21    
22     Rosas são vermelhas
23     Violetas são roxas
24     E eu comi batata frita hoje no almoço
```

E por último, salvou suas mudanças na branch 'minha\_primeira\_branch':

```bash
mathias$ git add .
mathias$ git commit -m "Fiz o merge da branch 'branch_para_fazer_merge_com_a_minha_primeira_branch' para a branch 'minha_primeira_branch'. A batata frita da mamãe tava muito boa hoje."
mathias$ git push origin minha_primeira_branch
```

Com isso, Mathias terminou seu segundo teste!

Até este capítulo, ele já relembrou como criar branches, o que são arquivos não monitorados, como saber se o repositório está atualizado ou não, o que são branches e como utilizar o `git merge` para mesclar duas branches.

(Se você ainda não entendeu algum dos conceitos acima, pesquise e/ou faça mais testes e veja as coisas funcionando!)

### Capítulo 5: De volta para o futuro

Para o último teste, Mathias queria testar algumas das funcionalidades do comando mais amplo do `git`: o `git checkout`!

Esse comando é como um coringa da ferramenta, capaz de criar branches, voltar para o passado, voltar do passado e mover arquivos entre o espaço-tempo também! A documentação completa do comando pode ser acessada digitando `man git checkout` no terminal *bash* do Linux.

Para o terceiro e último teste de Mathias, a ideia é simples: utilizar o comando de diversas formas, para explorar o que ele é capaz de fazer.

Antes da primeira parte do teste, Mathias criou uma branch nova e um arquivo de teste nessa branch nova:

```bash
mathias$ git checkout -b usando_o_checkout
mathias$ touch testando_o_checkout.txt
mathias$ echo "Agora eu vou testar o checkout. Enquanto eu como batata." >> testando_o_checkout.txt
mathias$ git add .
mathias$ git commit -m "Agora estou testando o checkout enquanto eu como batata. E eu escrevi isso no arquivo também."
```

E depois, Mathias apagou o arquivo:

```bash
mathias$ rm testando_o_checkout.txt
mathias$ git add .
mathias$ git commit -m "Apaguei o arquivo. E terminei de comer a batata."
```

Agora, o objetivo de Mathias é pegar o arquivo apagado do passado. Para isso, ele precisa lembrar qual foi o commit em que ele apagou o arquivo (que foi esse último), e voltar no tempo para o commit antes dele deletar o arquivo. Para ver esses commits, basta executar o comando `git log`, e para voltar, basta executar o comando `git checkout <commit>`:

```bash
mathias$ git log
commit 1dfdf6127a031c057aa148293696afc6b92e099e
Author: ecomp <seu_email@email.com>
Date:   Tue Apr 24 15:00:05 2018 -0300

    Apaguei o arquivo. E terminei de comer a batata.

commit aad37650107d00e744b1e09c4d420be22205205d
Author: ecomp <seu_email@email.com>
Date:   Tue Apr 24 14:59:51 2018 -0300

    Agora estou testando o checkout enquanto eu como batata. E eu escrevi isso no arquivo também.

mathias$ git checkout aad37650107d00e744b1e09c4d420be22205205d
Note: checking out 'aad37650107d00e744b1e09c4d420be22205205d'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at aad3765... Agora estou testando o checkout enquanto eu como batata. E eu escrevi isso no arquivo também.

mathias$ git branch
* (HEAD detached at aad3765)
  usando_o_checkout
[...]
```

Conforme sugerido pelo próprio comando, se Mathias quiser voltar para o passado realmente, basta que ele execute `git checkout -b <nova_branch>` para que seu commit do passado seja salvo em uma nova branch.

Mas Mathias não é de ficar se arrependendo de suas escolhas e voltar ao passado. Ele enfrenta suas dificuldades e refaz o que for preciso!

Então, o que Mathias decidiu fazer é um pouco diferente: Ele resolveu voltar para o presente e simplesmente trazer seu arquivo do passado para o presente!

```bash
# Para voltar ao presente, é só voltar para a branch original
mathias$ git checkout usando_o_checkout
Previous HEAD position was aad3765... Agora estou testando o checkout enquanto eu como batata. E eu escrevi isso no arquivo também.
Switched to branch 'usando_o_checkout'

# Se voce colocar dois traços '--' após o checkout, você irá baixar um arquivo daquela branch, ao invés de mudar para ela! (O passado é uma branch também)
mathias$ git checkout aad37650107d00e744b1e09c4d420be22205205d -- testando_o_checkout.txt
mathias$ git add .
mathias$ git commit -m "Peguei o arquivo de volta. Me arrependi, mas eu não volto para o passado. Eu trago o passado para o presente. Por isso eu comprei mais batata."
```

Com isso, Mathias terminou seu segundo teste!

Até este último capítulo, ele já relembrou como criar branches, o que são arquivos não monitorados, como saber se o repositório está atualizado ou não, o que são branches, como utilizar o `git merge` para mesclar duas branches e como atravessar o espaço-tempo no mundo do `git`.

(Se você ainda não entendeu algum dos conceitos acima, pesquise e/ou faça mais testes e veja as coisas funcionando!)

## Atividade Pós-Roteiro

Agora você está preparado (ou não) para fazer sua atividade. Lembra das branches que apareceram quando você executou `git branch -r`?

Para esta atividade, você não será mais Mathias.

Você agora se tornou o detetive **Cherilóqui, o detetive que descobre o que ninguém descobriu**. Cherilóqui (9 anos) é primo de terceiro grau de Mathias (seu pai é primo do tio do pai do Cherilóqui), e ele também é um desbravador do mundo do git e sofredor com certificado.

Para lhe ajudar a encarnar a personagem, imagine como foi a vida de Cherilóqui até aqui, desde seu nascimento em 31 de Abril de 1968 (+ 41). Seus 9 anos de vida lhe proporcionaram inúmeras experiências, e todos os casos que ele resolveu até agora complementaram ainda mais seu repertório de frases de efeito.

Cherilóqui agora irá resolver mais um mistério, descrito abaixo.

**Após ler a história, vá para a branch `regras` para começar a atividade!**

## O Desaparecimento

  Em um dia qualquer, em Abril de 2018, por volta de cinco horas da tarde, Cherilóqui recebeu um chamado em seu smartphone vermelho. Era um pedido de socorro urgente. Cherilóqui correu para o local: **a sala de estar de sua casa**. Quem ligou foi sua tia, Creusonéte, que estava passando alguns dias na casa de sua irmã, mãe de Cherilóqui.
  
  Ela explicou a situação: **seu celular havia desaparecido!**

  Cherilóqui pediu mais detalhes sobre onde a vítima passou no dia do incidente (hoje). Ela contou que havia saído de manhã, por volta das nove horas (09h00), mas não levou o celular. Quando voltou, por volta de uma hora da tarde (13h00), pegou seu celular na sala, o colocou em seu bolso e foi direto para a cozinha ajudar sua irmã a terminar de preparar o almoço.
  
  O almoço ficou pronto por volta de duas horas (14h00), e tinha batata frita. A vítima foi então chamar seu sobrinho, que estava no quarto, para almoçar. Seu quarto ficava no primeiro andar, logo após subir uma escada posicionada na sala de estar. Antes de subir, colocou seu celular em uma mesa baixa que ficava próxima às escadas sem motivo aparente.
  
  Depois de almoçar, já eram duas e meia da tarde (14h30). Creusonéte e sua irmã resolveram cochilar na rede fora de casa. Dormiram por duas horas (16h30), e quando acordaram, resolveram ir preparar o café da tarde: pipoca. Depois de preparar a pipoca, Creusonéte foi chamar seu sobrinho, mas estava com preguiça de subir as escadas de novo. Então, ela resolveu ligar para ele pelo aplicativo QuePassapp, mas percebeu que seu celular não estava mais na mesa da sala!
  
  Ela procurou pelo celular furiosamente nas proximidades, mas sem sucesso. Depois de desistir, lembrou que ainda não tinha chamado seu sobrinho e pediu emprestado o celular de sua irmã para ligar para ele.
  
  Esse foi o depoimento da vítima. Cherilóqui, enquanto comia sua pipoca, absorvia todos os mínimos detalhes da situação. Ele concluiu que a pipoca estava com pouco sal!

  Depois de comer, ele chamou seu cão-ajudante Brabo, que veio correndo de sua casinha do lado de fora até a sala. Tomando uma pose estilosa ao lado de seu companheiro, Cherilóqui disse:

- Nada tema, cara senhora! Cherilóqui e seu magnífico cão-ajudante Brabo vão resolver esse mistério!

